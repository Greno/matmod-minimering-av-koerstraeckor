function outpoint = bestpoint(points, weights, factories)
bestp = [0,0];
bestsum = 1000000000;
%BESTPOINT Räknar ut bästa punkt
for x = linspace(0,1, 100)
    for y = linspace(0,1, 100)
        sum = 0;
        for p = 1:length(points)
            distances(1) = hypot(points(p,1)-x, points(p,2)-y);

            for i = 1:length(factories)
                distances(i+1) = hypot(factories(i,1)-points(p,1), factories(i,2)-points(p,2));
            end
            
            sum = sum + min(distances)*weights(p);
            

        end

        if (sum < bestsum)
            bestsum = sum;
            bestp = [x, y];
        end
       
    end
end

outpoint = bestp;
end

