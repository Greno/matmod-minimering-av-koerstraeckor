
factories = rand(3, 2);
points = rand(8,2);
weights = rand(8,1) + 0.1;

bestp = bestpoint(points, weights, factories);
lines = bestlines(points, weights, factories);
p1 = 0;
p2 = 0;
p3 = 0;
axis([0 1 0 1]);
xlabel('Position x-led (km)');
ylabel('Position y-led (km)');

for p = 1:length(points)
    hold on
    p1 = plot(points(p, 1), points(p, 2), '.', 'MarkerSize', weights(p)*50, 'Color', 'blue');
end 

for p = 1:length(lines)*0.5
    p2 = plot([lines(1, p*2 -1), lines(1, p*2)], [lines(2, p*2 -1), lines(2, p*2)], 'Color', 'black');
end

for p = 1:length(factories)
    p3 = plot(factories(p,1), factories(p,2), 'o', 'Color', 'red');
    p4 = plot([factories(p,1), bestp(1)], [factories(p,2), bestp(2)], '--', 'Color', '#808080');
end
p5 = plot(bestp(1), bestp(2), 'x', 'MarkerSize', 10, 'LineWidth', 2);

legend([p5 p1 p3 p2 p4], {'Mellanlager', 'Grossist', 'Fabrik', 'Väg', 'Räls'})
title('Placering av mellanlager')
