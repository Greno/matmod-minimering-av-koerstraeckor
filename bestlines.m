function lines = bestlines(points, weights, factories)
bestp = [0,0];
bestsum = 1000000000;
%BESTPOINT Räknar ut bästa punkt
for x = linspace(0,1, 100)
    for y = linspace(0,1, 100)
        sum = 0;
        multilines = [];
        for p = 1:length(points)
            line = [];
            distances(1) = hypot(points(p,1)-x, points(p,2)-y);

            for i = 1:length(factories)
                distances(i+1) = hypot(factories(i,1)-points(p,1), factories(i,2)-points(p,2));
            end
            
            sum = sum + min(distances)*weights(p);
            % Räkna ut linjer
            [m, I] = min(distances);
            if (I == 1)
                line = [points(p,1), x; points(p,2), y];
            else
                line = [points(p,1), factories(I-1,1); points(p,2), factories(I-1,2)];
            end
            multilines = [multilines, line];
        end

        if (sum < bestsum)
            bestsum = sum;
            lines = multilines;
            bestp = [x, y];
        end
       
    end
end

outpoint = bestp;
end

